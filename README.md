# pt-proj-test

This is a test project for [pipeline-trigger](https://gitlab.com/finestructure/pipeline-trigger).

pipeline-trigger will make changes and trigger pipelines in this project as part of its test suite.
